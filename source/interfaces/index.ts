import {Context} from 'koa'
import {HttpMethod} from '../constants'

export interface optionsInterface {
  prefix?: string
  connector: any
  routeName?: (ClassName: string) => string
  modelName?: (ClassName: string) => string
  db?: any
  errorHandler?: (ctx: Context) => any
}

export interface RouteInterface {

}

export interface DecoratorInterface {
  (target: RouteInterface, propertyKey: string, descriptor: PropertyDescriptor): void
}

export interface MiddlewareInternalInterface {
  (context: Context, next: () => Promise<any>): Promise<any>
}

export interface MiddlewareInterface {
  (className: string, action: string): MiddlewareInternalInterface
}

export interface MapItemInterface {
  routes?: string[]
  middleware?: MiddlewareInternalInterface[]
  method?: HttpMethod
  propertyKey?: string
  className?: string
}

export interface queryCriteriaInterface {
  [key: string]: any
}

export interface ModelConnectorInterface {
  model: any
  find: (schemaName: string, criteria: queryCriteriaInterface) => PromiseLike<any>
  findById: (schemaName: string, id: any, criteria: queryCriteriaInterface) => PromiseLike<any>
  create: (schemaName: string, data: any) => PromiseLike<any>
  update: (schemaName: string, id: any, criteria: queryCriteriaInterface, data: any) => PromiseLike<any>
  delete: (schemaName: string, id: any, criteria: queryCriteriaInterface) => PromiseLike<any>
}