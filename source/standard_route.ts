import {Context} from 'koa'
// import * as model from '../models'
import {Interface} from './dekoa'

export interface StandardRouteInterface {
  list? (ctx: Context): Promise<any>
  get? (ctx: Context): Promise<any>
  create? (ctx: Context): Promise<any>
  update? (ctx: Context): Promise<any>
  delete? (ctx: Context): Promise<any>
}

export class StandardRoute implements StandardRouteInterface {

  public async list (ctx: Context): Promise<any> {
    let findCriteria: any = ctx.state.findCriteria || {}
    try {
      ctx.body = await ctx.state.dekoa.connector.find(ctx.state.dekoa.modelName, findCriteria)
    } catch (err) {
      ctx.throw(err)
    }
    return ctx.body
  }

  public async get (ctx: Context) {
    if (!ctx.params.id) {
      ctx.throw('MISSING_ID', 500)
    } else {
      let findCriteria: any = ctx.state.findCriteria || {}
      try {
        ctx.body = await ctx.state.dekoa.connector.findById(ctx.state.dekoa.modelName, ctx.params.id, findCriteria)
      } catch (err) {
        ctx.throw(err)
      }
    }
    return ctx.body
  }

  public async create (ctx: Context) {
    try {
      ctx.body = await ctx.state.dekoa.connector.create(ctx.state.dekoa.modelName, ctx.request.body)
    } catch (err) {
      ctx.throw(err)
    }
    return ctx.body
  }

  public async update(ctx: Context) {
    if (!ctx.params.id) {
      ctx.throw('MISSING_ID', 500)
    } else {
      let findCriteria: any = ctx.state.findCriteria || {}
      try {
        ctx.body = await ctx.state.dekoa.connector.update(ctx.state.dekoa.modelName, ctx.params.id, findCriteria, ctx.request.body)
      } catch (err) {
        ctx.throw(err)
      }
    }
  }

  public async delete (ctx: Context) {
    if (!ctx.params.id) {
      ctx.throw('MISSING_ID', 500)
    } else {
      let findCriteria: any = ctx.state.findCriteria || {}
      try {
        await ctx.state.dekoa.connector.delete(ctx.state.dekoa.modelName, ctx.params.id, findCriteria)
        ctx.body = {status: 'OK'}
      } catch (err) {
        ctx.throw(err)
      }
    }
    return ctx.body
  }
}
