class Mongoose {
  model: any
  constructor (model: any) {
    this.model = model
  }

  find (schema: string, criteria: any) {
    return this.model[schema].find(criteria).exec()
  }

  findById (schema: string, id: any, criteria: any) {
    let opt = Object.assign({_id: id}, criteria)
    return this.model[schema].findOne(opt).exec()
  }

  create (schema: string, data: any) {
    return new this.model[schema](data).save()
  }

  update (schema: string, id: string, criteria: any, data: any) {
    let opt = Object.assign({_id: id}, criteria)
    return this.model[schema].update(opt, data)
  }

  delete (schema: string, id: string, criteria: any) {
    let opt = Object.assign({_id: id}, criteria)
    return this.model[schema].remove(opt)
  }
}

export {Mongoose}