import {queryCriteriaInterface, ModelConnectorInterface} from '../interfaces'

class Memory implements ModelConnectorInterface {
  model: any
  correlativeId: any

  constructor () {
    this.model = {}
    this.correlativeId = {}
  }

  _filter (data: any[], criteria: queryCriteriaInterface) {
    let criteriaKeys = Object.keys(criteria)
    let resp:any[] = [];
    (data || []).forEach((item) => {
      let ok = criteriaKeys.every((key: string): boolean => {
        return item[key] == criteria[key]
      })
      if (ok) resp.push(item)
    })
    return resp
  }

  async find (schema: string, criteria: queryCriteriaInterface) {
    return new Promise((resolve: any, reject: any) => {
      resolve(this._filter(this.model[schema], criteria))
    })
  }

  async findById (schema: string, id: any, criteria: any) {
    let opt = Object.assign({}, criteria, {id: id})
    return new Promise((resolve: any, reject: any) => {
      resolve(this._filter(this.model[schema], opt)[0])
    })
  }

  async create (schema: string, data: any) {
    let id: number
    if (this.correlativeId[schema]) {
      id = ++this.correlativeId[schema]
    } else {
      id = this.correlativeId[schema] = 1
      this.model[schema] = []
    }
    let _data = Object.assign({}, data, {id: id})
    return new Promise((resolve: any, reject: any) => {
      this.model[schema].push(_data)
      resolve(_data)
    })
  }

  async update (schema: string, id: string, criteria: queryCriteriaInterface, data: any) {
    let opt = Object.assign({}, criteria, {id: id})
    delete data.id
    return new Promise((resolve: any, reject: any) => {
      let old = this._filter(this.model[schema], opt);
      (old || []).forEach((item: any) => {
        Object.assign(item, data)
      })
      resolve(old[0])
    })
  }

  async delete (schema: string, id: string, criteria: queryCriteriaInterface) {
    let opt = Object.assign({}, criteria, {id: id})
    return new Promise((resolve: any, reject: any) => {
      let deleteIds = this._filter(this.model[schema], opt).map((item) => item.id);
      this.model[schema].forEach((item: any, idx: number) => {
        if (deleteIds.indexOf(item.id) !== -1) this.model[schema].splice(idx, 1)
      })
      resolve(deleteIds.length)
    })
  }
}

export {Memory}