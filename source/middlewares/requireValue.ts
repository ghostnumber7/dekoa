import {Context} from 'koa'
import {Interface} from '../dekoa'


function check (obj: any, path: string[]): boolean {
  if (path.length) {
    const part = path.splice(0, 1)[0]
    if (obj[part] !== null && obj[part] !== undefined) {
      return check(obj[part], path)
    }
    return false
  }
  return true
}

export function RequireValue (path: string | string[]) : Interface.MiddlewareInterface {
  if (!(path && (typeof path === 'string' || Array.isArray(path)))) {
    throw new Error('String or Array required for value `path`')
  }
  let splitedPaths: string[][]
  let splitedPath: string[]

  if (Array.isArray(path)) {
    splitedPaths = path.map((part) => part.split('.'))
  } else {
    splitedPath = path.split('.')
  }
  return <Interface.MiddlewareInterface>((className, action) => {
    return (ctx, next) => {
      let pass = true
      if (splitedPaths) {
        if (!splitedPaths.every((part: string[]) => {
          return check(ctx, [].concat.apply([], part))
        })) pass = false
      } else {
        if (!check(ctx, [].concat.apply([], splitedPath))) pass = false
      }

      if (!pass) return ctx.throw(new Error(`REQUIRED_VALUE_NOT_PRESENT`), 412)
      return next() // important! If middlewares don't return a promise they don't work as espected
    }
  })
}
