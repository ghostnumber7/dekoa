export enum HttpMethod {
  GET = <any>'get',
  POST = <any>'post',
  PUT = <any>'put',
  DELETE = <any>'delete'
}