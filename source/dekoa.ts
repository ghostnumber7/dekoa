import * as KoaRouter from 'koa-router'
import * as path from 'path'
import {Context as KoaContext} from 'koa'
import * as Interface from './interfaces'
import * as Constant from './constants'
let InstanceDecoratorMap = {}
let InstanceBasePaths = {}
import * as Database from './databases'
import * as Middleware from './middlewares'

export * from './standard_route'
export {Interface, Constant, Database, Middleware}


const defaultOptions: Interface.optionsInterface = {
  prefix: '',
  connector: new Database.Memory(),
  routeName: (ClassName): string => ClassName.toLowerCase(),
  modelName: (ClassName): string => ClassName.toLowerCase()
}

export default function Dekoa (app: any, routes: any[], options?: Interface.optionsInterface) {
  const opt = Object.assign(defaultOptions, options)
  const router = new KoaRouter({prefix: opt.prefix})
  if (typeof opt.errorHandler === 'function') {
    app.use(async (ctx: KoaContext, next: any) => {
      try {
        return await next()
      } catch (err) {
        ctx.state.error = err
        app.emit('error', err, this)
        return opt.errorHandler(ctx)
      }
    })
  }
  routes.map((route) => setRouter(opt, router, new route()))
  app.use(router.routes())
  return router.allowedMethods()
}

function setRouter (opt: Interface.optionsInterface, router: KoaRouter, instance: Interface.RouteInterface): Interface.RouteInterface {
  let item = getMapItem(instance.constructor.name)
  Object.keys(item || {}).map((key: string) => {
    let obj: Interface.MapItemInterface = (<any>item)[key];
    (obj.routes || []).forEach((_route: string) => {
      const className = (<any>InstanceBasePaths)[obj.className] || obj.className
      let finalRotue = path.join(
        '/',
        opt.routeName(className),
        _route
      );
      (<any>router)[obj.method](finalRotue, getContextData(className, obj, opt), ...(obj.middleware || []), (<any>instance)[obj.propertyKey].bind(instance))
      console.log(`InstanceDecoratorMapped: ${obj.method} ${((<any>router).opts || {}).prefix || ''}${finalRotue}`) // TODO: Notificar tipo debug
    })
  })
  return instance
}

function getMapItem (className: string, method?: Constant.HttpMethod, propertyKey?: string): Interface.MapItemInterface {
  if ((<any>InstanceDecoratorMap)[className] === undefined) (<any>InstanceDecoratorMap)[className] = {}
  if ('undefined' === typeof propertyKey) return (<any>InstanceDecoratorMap)[className]
  const methodKey = `${method}#${propertyKey}`
  if ((<any>InstanceDecoratorMap)[className][methodKey] === undefined) (<any>InstanceDecoratorMap)[className][methodKey] = {}
  return (<any>InstanceDecoratorMap)[className][methodKey]
}

function getBaseName (className: string): string {
  return (<any>InstanceBasePaths)[className] || className
}

function getContextData (className: string, obj: Interface.MapItemInterface, opt: Interface.optionsInterface): Interface.MiddlewareInternalInterface {
  const modelName = opt.modelName(className)
  return function (ctx: KoaContext, next: any): Promise<any> {
    ctx.state.dekoa = {
      modelName: modelName,
      connector: opt.connector,
      className: className,
      method: obj.propertyKey,
      route: ctx.originalUrl
    }
    ctx.state.model = opt.connector.model || {}
    return next()
  }
}

export class Router {
  static basePath (route: string): (target: any) => void {
    return (target: any) => {
      (<any>InstanceBasePaths)[target.name] = route
    }
  }
  static route (method: Constant.HttpMethod, route: string | [string], ...middleware: Interface.MiddlewareInterface[]): Interface.DecoratorInterface {
    return (target: Interface.RouteInterface, propertyKey: string, descriptor: PropertyDescriptor) => {
      let className = target.constructor.name
      let item = getMapItem(className, method, propertyKey)
      let routes: string[] = [].concat.apply([], [route])
      item.className = className
      item.routes = routes
      item.middleware = middleware.map((_middleware) => _middleware(className, propertyKey))
      item.method = method
      item.propertyKey = propertyKey
    }
  }
  static get (route: string | [string], ...middleware: Interface.MiddlewareInterface[]): Interface.DecoratorInterface {
    return Router.route(Constant.HttpMethod.GET, route, ...middleware)
  }
  static post (route: string | [string], ...middleware: Interface.MiddlewareInterface[]): Interface.DecoratorInterface {
    return Router.route(Constant.HttpMethod.POST, route, ...middleware)
  }
  static put (route: string | [string], ...middleware: Interface.MiddlewareInterface[]): Interface.DecoratorInterface {
    return Router.route(Constant.HttpMethod.PUT, route, ...middleware)
  }
  static delete (route: string | [string], ...middleware: Interface.MiddlewareInterface[]): Interface.DecoratorInterface {
    return Router.route(Constant.HttpMethod.DELETE, route, ...middleware)
  }
}