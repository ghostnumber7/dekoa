"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Mongoose = function () {
    function Mongoose(model) {
        _classCallCheck(this, Mongoose);

        this.model = model;
    }

    _createClass(Mongoose, [{
        key: "find",
        value: function find(schema, criteria) {
            return this.model[schema].find(criteria).exec();
        }
    }, {
        key: "findById",
        value: function findById(schema, id, criteria) {
            var opt = Object.assign({ _id: id }, criteria);
            return this.model[schema].findOne(opt).exec();
        }
    }, {
        key: "create",
        value: function create(schema, data) {
            return new this.model[schema](data).save();
        }
    }, {
        key: "update",
        value: function update(schema, id, criteria, data) {
            var opt = Object.assign({ _id: id }, criteria);
            return this.model[schema].update(opt, data);
        }
    }, {
        key: "delete",
        value: function _delete(schema, id, criteria) {
            var opt = Object.assign({ _id: id }, criteria);
            return this.model[schema].remove(opt);
        }
    }]);

    return Mongoose;
}();

exports.Mongoose = Mongoose;
//# sourceMappingURL=mongoose.js.map