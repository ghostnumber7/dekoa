"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
            try {
                step(generator.next(value));
            } catch (e) {
                reject(e);
            }
        }
        function rejected(value) {
            try {
                step(generator.throw(value));
            } catch (e) {
                reject(e);
            }
        }
        function step(result) {
            result.done ? resolve(result.value) : new P(function (resolve) {
                resolve(result.value);
            }).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};

var Memory = function () {
    function Memory() {
        _classCallCheck(this, Memory);

        this.model = {};
        this.correlativeId = {};
    }

    _createClass(Memory, [{
        key: "_filter",
        value: function _filter(data, criteria) {
            var criteriaKeys = Object.keys(criteria);
            var resp = [];
            (data || []).forEach(function (item) {
                var ok = criteriaKeys.every(function (key) {
                    return item[key] == criteria[key];
                });
                if (ok) resp.push(item);
            });
            return resp;
        }
    }, {
        key: "find",
        value: function find(schema, criteria) {
            return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee() {
                var _this = this;

                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                return _context.abrupt("return", new Promise(function (resolve, reject) {
                                    resolve(_this._filter(_this.model[schema], criteria));
                                }));

                            case 1:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));
        }
    }, {
        key: "findById",
        value: function findById(schema, id, criteria) {
            return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee2() {
                var _this2 = this;

                var opt;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                opt = Object.assign({}, criteria, { id: id });
                                return _context2.abrupt("return", new Promise(function (resolve, reject) {
                                    resolve(_this2._filter(_this2.model[schema], opt)[0]);
                                }));

                            case 2:
                            case "end":
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));
        }
    }, {
        key: "create",
        value: function create(schema, data) {
            return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee3() {
                var _this3 = this;

                var id, _data;

                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                id = void 0;

                                if (this.correlativeId[schema]) {
                                    id = ++this.correlativeId[schema];
                                } else {
                                    id = this.correlativeId[schema] = 1;
                                    this.model[schema] = [];
                                }
                                _data = Object.assign({}, data, { id: id });
                                return _context3.abrupt("return", new Promise(function (resolve, reject) {
                                    _this3.model[schema].push(_data);
                                    resolve(_data);
                                }));

                            case 4:
                            case "end":
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));
        }
    }, {
        key: "update",
        value: function update(schema, id, criteria, data) {
            return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
                var _this4 = this;

                var opt;
                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                opt = Object.assign({}, criteria, { id: id });

                                delete data.id;
                                return _context4.abrupt("return", new Promise(function (resolve, reject) {
                                    var old = _this4._filter(_this4.model[schema], opt);
                                    (old || []).forEach(function (item) {
                                        Object.assign(item, data);
                                    });
                                    resolve(old[0]);
                                }));

                            case 3:
                            case "end":
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));
        }
    }, {
        key: "delete",
        value: function _delete(schema, id, criteria) {
            return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee5() {
                var _this5 = this;

                var opt;
                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                opt = Object.assign({}, criteria, { id: id });
                                return _context5.abrupt("return", new Promise(function (resolve, reject) {
                                    var deleteIds = _this5._filter(_this5.model[schema], opt).map(function (item) {
                                        return item.id;
                                    });
                                    _this5.model[schema].forEach(function (item, idx) {
                                        if (deleteIds.indexOf(item.id) !== -1) _this5.model[schema].splice(idx, 1);
                                    });
                                    resolve(deleteIds.length);
                                }));

                            case 2:
                            case "end":
                                return _context5.stop();
                        }
                    }
                }, _callee5, this);
            }));
        }
    }]);

    return Memory;
}();

exports.Memory = Memory;
//# sourceMappingURL=memory.js.map