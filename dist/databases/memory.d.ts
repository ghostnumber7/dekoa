import { queryCriteriaInterface, ModelConnectorInterface } from '../interfaces';
declare class Memory implements ModelConnectorInterface {
    model: any;
    correlativeId: any;
    constructor();
    _filter(data: any[], criteria: queryCriteriaInterface): any[];
    find(schema: string, criteria: queryCriteriaInterface): Promise<{}>;
    findById(schema: string, id: any, criteria: any): Promise<{}>;
    create(schema: string, data: any): Promise<{}>;
    update(schema: string, id: string, criteria: queryCriteriaInterface, data: any): Promise<{}>;
    delete(schema: string, id: string, criteria: queryCriteriaInterface): Promise<{}>;
}
export { Memory };
