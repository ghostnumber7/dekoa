declare class Mongoose {
    model: any;
    constructor(model: any);
    find(schema: string, criteria: any): any;
    findById(schema: string, id: any, criteria: any): any;
    create(schema: string, data: any): any;
    update(schema: string, id: string, criteria: any, data: any): any;
    delete(schema: string, id: string, criteria: any): any;
}
export { Mongoose };
