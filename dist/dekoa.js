"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
            try {
                step(generator.next(value));
            } catch (e) {
                reject(e);
            }
        }
        function rejected(value) {
            try {
                step(generator.throw(value));
            } catch (e) {
                reject(e);
            }
        }
        function step(result) {
            result.done ? resolve(result.value) : new P(function (resolve) {
                resolve(result.value);
            }).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
function __export(m) {
    for (var p in m) {
        if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
}
var KoaRouter = require('koa-router');
var path = require('path');
var Interface = require('./interfaces');
exports.Interface = Interface;
var Constant = require('./constants');
exports.Constant = Constant;
var InstanceDecoratorMap = {};
var InstanceBasePaths = {};
var Database = require('./databases');
exports.Database = Database;
var Middleware = require('./middlewares');
exports.Middleware = Middleware;
__export(require('./standard_route'));
var defaultOptions = {
    prefix: '',
    connector: new Database.Memory(),
    routeName: function routeName(ClassName) {
        return ClassName.toLowerCase();
    },
    modelName: function modelName(ClassName) {
        return ClassName.toLowerCase();
    }
};
function Dekoa(app, routes, options) {
    var _this = this;

    var opt = Object.assign(defaultOptions, options);
    var router = new KoaRouter({ prefix: opt.prefix });
    if (typeof opt.errorHandler === 'function') {
        app.use(function (ctx, next) {
            return __awaiter(_this, void 0, void 0, regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _context.next = 3;
                                return next();

                            case 3:
                                return _context.abrupt('return', _context.sent);

                            case 6:
                                _context.prev = 6;
                                _context.t0 = _context['catch'](0);

                                ctx.state.error = _context.t0;
                                app.emit('error', _context.t0, this);
                                return _context.abrupt('return', opt.errorHandler(ctx));

                            case 11:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 6]]);
            }));
        });
    }
    routes.map(function (route) {
        return setRouter(opt, router, new route());
    });
    app.use(router.routes());
    return router.allowedMethods();
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Dekoa;
function setRouter(opt, router, instance) {
    var item = getMapItem(instance.constructor.name);
    Object.keys(item || {}).map(function (key) {
        var obj = item[key];
        (obj.routes || []).forEach(function (_route) {
            var className = InstanceBasePaths[obj.className] || obj.className;
            var finalRotue = path.join('/', opt.routeName(className), _route);
            router[obj.method].apply(router, [finalRotue, getContextData(className, obj, opt)].concat(_toConsumableArray(obj.middleware || []), [instance[obj.propertyKey].bind(instance)]));
            console.log('InstanceDecoratorMapped: ' + obj.method + ' ' + ((router.opts || {}).prefix || '') + finalRotue); // TODO: Notificar tipo debug
        });
    });
    return instance;
}
function getMapItem(className, method, propertyKey) {
    if (InstanceDecoratorMap[className] === undefined) InstanceDecoratorMap[className] = {};
    if ('undefined' === typeof propertyKey) return InstanceDecoratorMap[className];
    var methodKey = method + '#' + propertyKey;
    if (InstanceDecoratorMap[className][methodKey] === undefined) InstanceDecoratorMap[className][methodKey] = {};
    return InstanceDecoratorMap[className][methodKey];
}
function getBaseName(className) {
    return InstanceBasePaths[className] || className;
}
function getContextData(className, obj, opt) {
    var modelName = opt.modelName(className);
    return function (ctx, next) {
        ctx.state.dekoa = {
            modelName: modelName,
            connector: opt.connector,
            className: className,
            method: obj.propertyKey,
            route: ctx.originalUrl
        };
        ctx.state.model = opt.connector.model || {};
        return next();
    };
}

var Router = function () {
    function Router() {
        _classCallCheck(this, Router);
    }

    _createClass(Router, null, [{
        key: 'basePath',
        value: function basePath(route) {
            return function (target) {
                InstanceBasePaths[target.name] = route;
            };
        }
    }, {
        key: 'route',
        value: function route(method, _route2) {
            for (var _len = arguments.length, middleware = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
                middleware[_key - 2] = arguments[_key];
            }

            return function (target, propertyKey, descriptor) {
                var className = target.constructor.name;
                var item = getMapItem(className, method, propertyKey);
                var routes = [].concat.apply([], [_route2]);
                item.className = className;
                item.routes = routes;
                item.middleware = middleware.map(function (_middleware) {
                    return _middleware(className, propertyKey);
                });
                item.method = method;
                item.propertyKey = propertyKey;
            };
        }
    }, {
        key: 'get',
        value: function get(route) {
            for (var _len2 = arguments.length, middleware = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
                middleware[_key2 - 1] = arguments[_key2];
            }

            return Router.route.apply(Router, [Constant.HttpMethod.GET, route].concat(middleware));
        }
    }, {
        key: 'post',
        value: function post(route) {
            for (var _len3 = arguments.length, middleware = Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
                middleware[_key3 - 1] = arguments[_key3];
            }

            return Router.route.apply(Router, [Constant.HttpMethod.POST, route].concat(middleware));
        }
    }, {
        key: 'put',
        value: function put(route) {
            for (var _len4 = arguments.length, middleware = Array(_len4 > 1 ? _len4 - 1 : 0), _key4 = 1; _key4 < _len4; _key4++) {
                middleware[_key4 - 1] = arguments[_key4];
            }

            return Router.route.apply(Router, [Constant.HttpMethod.PUT, route].concat(middleware));
        }
    }, {
        key: 'delete',
        value: function _delete(route) {
            for (var _len5 = arguments.length, middleware = Array(_len5 > 1 ? _len5 - 1 : 0), _key5 = 1; _key5 < _len5; _key5++) {
                middleware[_key5 - 1] = arguments[_key5];
            }

            return Router.route.apply(Router, [Constant.HttpMethod.DELETE, route].concat(middleware));
        }
    }]);

    return Router;
}();

exports.Router = Router;
//# sourceMappingURL=dekoa.js.map