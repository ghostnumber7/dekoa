"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
            try {
                step(generator.next(value));
            } catch (e) {
                reject(e);
            }
        }
        function rejected(value) {
            try {
                step(generator.throw(value));
            } catch (e) {
                reject(e);
            }
        }
        function step(result) {
            result.done ? resolve(result.value) : new P(function (resolve) {
                resolve(result.value);
            }).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};

var StandardRoute = function () {
    function StandardRoute() {
        _classCallCheck(this, StandardRoute);
    }

    _createClass(StandardRoute, [{
        key: 'list',
        value: function list(ctx) {
            return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee() {
                var findCriteria;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                findCriteria = ctx.state.findCriteria || {};
                                _context.prev = 1;
                                _context.next = 4;
                                return ctx.state.dekoa.connector.find(ctx.state.dekoa.modelName, findCriteria);

                            case 4:
                                ctx.body = _context.sent;
                                _context.next = 10;
                                break;

                            case 7:
                                _context.prev = 7;
                                _context.t0 = _context['catch'](1);

                                ctx.throw(_context.t0);

                            case 10:
                                return _context.abrupt('return', ctx.body);

                            case 11:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[1, 7]]);
            }));
        }
    }, {
        key: 'get',
        value: function get(ctx) {
            return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee2() {
                var findCriteria;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                if (ctx.params.id) {
                                    _context2.next = 4;
                                    break;
                                }

                                ctx.throw('MISSING_ID', 500);
                                _context2.next = 14;
                                break;

                            case 4:
                                findCriteria = ctx.state.findCriteria || {};
                                _context2.prev = 5;
                                _context2.next = 8;
                                return ctx.state.dekoa.connector.findById(ctx.state.dekoa.modelName, ctx.params.id, findCriteria);

                            case 8:
                                ctx.body = _context2.sent;
                                _context2.next = 14;
                                break;

                            case 11:
                                _context2.prev = 11;
                                _context2.t0 = _context2['catch'](5);

                                ctx.throw(_context2.t0);

                            case 14:
                                return _context2.abrupt('return', ctx.body);

                            case 15:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[5, 11]]);
            }));
        }
    }, {
        key: 'create',
        value: function create(ctx) {
            return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee3() {
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                _context3.next = 3;
                                return ctx.state.dekoa.connector.create(ctx.state.dekoa.modelName, ctx.request.body);

                            case 3:
                                ctx.body = _context3.sent;
                                _context3.next = 9;
                                break;

                            case 6:
                                _context3.prev = 6;
                                _context3.t0 = _context3['catch'](0);

                                ctx.throw(_context3.t0);

                            case 9:
                                return _context3.abrupt('return', ctx.body);

                            case 10:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[0, 6]]);
            }));
        }
    }, {
        key: 'update',
        value: function update(ctx) {
            return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
                var findCriteria;
                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                if (ctx.params.id) {
                                    _context4.next = 4;
                                    break;
                                }

                                ctx.throw('MISSING_ID', 500);
                                _context4.next = 14;
                                break;

                            case 4:
                                findCriteria = ctx.state.findCriteria || {};
                                _context4.prev = 5;
                                _context4.next = 8;
                                return ctx.state.dekoa.connector.update(ctx.state.dekoa.modelName, ctx.params.id, findCriteria, ctx.request.body);

                            case 8:
                                ctx.body = _context4.sent;
                                _context4.next = 14;
                                break;

                            case 11:
                                _context4.prev = 11;
                                _context4.t0 = _context4['catch'](5);

                                ctx.throw(_context4.t0);

                            case 14:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[5, 11]]);
            }));
        }
    }, {
        key: 'delete',
        value: function _delete(ctx) {
            return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee5() {
                var findCriteria;
                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                if (ctx.params.id) {
                                    _context5.next = 4;
                                    break;
                                }

                                ctx.throw('MISSING_ID', 500);
                                _context5.next = 14;
                                break;

                            case 4:
                                findCriteria = ctx.state.findCriteria || {};
                                _context5.prev = 5;
                                _context5.next = 8;
                                return ctx.state.dekoa.connector.delete(ctx.state.dekoa.modelName, ctx.params.id, findCriteria);

                            case 8:
                                ctx.body = { status: 'OK' };
                                _context5.next = 14;
                                break;

                            case 11:
                                _context5.prev = 11;
                                _context5.t0 = _context5['catch'](5);

                                ctx.throw(_context5.t0);

                            case 14:
                                return _context5.abrupt('return', ctx.body);

                            case 15:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[5, 11]]);
            }));
        }
    }]);

    return StandardRoute;
}();

exports.StandardRoute = StandardRoute;
//# sourceMappingURL=standard_route.js.map