"use strict";

(function (HttpMethod) {
    HttpMethod[HttpMethod["GET"] = 'get'] = "GET";
    HttpMethod[HttpMethod["POST"] = 'post'] = "POST";
    HttpMethod[HttpMethod["PUT"] = 'put'] = "PUT";
    HttpMethod[HttpMethod["DELETE"] = 'delete'] = "DELETE";
})(exports.HttpMethod || (exports.HttpMethod = {}));
var HttpMethod = exports.HttpMethod;
//# sourceMappingURL=index.js.map