export declare enum HttpMethod {
    GET,
    POST,
    PUT,
    DELETE,
}
