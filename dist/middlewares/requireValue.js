"use strict";

function check(obj, path) {
    if (path.length) {
        var part = path.splice(0, 1)[0];
        if (obj[part] !== null && obj[part] !== undefined) {
            return check(obj[part], path);
        }
        return false;
    }
    return true;
}
function RequireValue(path) {
    if (!(path && (typeof path === 'string' || Array.isArray(path)))) {
        throw new Error('String or Array required for value `path`');
    }
    var splitedPaths = void 0;
    var splitedPath = void 0;
    if (Array.isArray(path)) {
        splitedPaths = path.map(function (part) {
            return part.split('.');
        });
    } else {
        splitedPath = path.split('.');
    }
    return function (className, action) {
        return function (ctx, next) {
            var pass = true;
            if (splitedPaths) {
                if (!splitedPaths.every(function (part) {
                    return check(ctx, [].concat.apply([], part));
                })) pass = false;
            } else {
                if (!check(ctx, [].concat.apply([], splitedPath))) pass = false;
            }
            if (!pass) return ctx.throw(new Error('REQUIRED_VALUE_NOT_PRESENT'), 412);
            return next(); // important! If middlewares don't return a promise they don't work as espected
        };
    };
}
exports.RequireValue = RequireValue;
//# sourceMappingURL=requireValue.js.map