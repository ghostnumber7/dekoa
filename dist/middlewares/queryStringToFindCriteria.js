"use strict";

exports.QueryStringToFindCriteria = function (className, action) {
    return function (ctx, next) {
        // Criteria is used to filter queries
        ctx.state.findCriteria = ctx.request.query;
        return next(); // important! If middlewares don't return a promise they don't work as espected
    };
};
//# sourceMappingURL=queryStringToFindCriteria.js.map