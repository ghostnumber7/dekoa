import * as KoaRouter from 'koa-router';
import * as Interface from './interfaces';
import * as Constant from './constants';
import * as Database from './databases';
import * as Middleware from './middlewares';
export * from './standard_route';
export { Interface, Constant, Database, Middleware };
export default function Dekoa(app: any, routes: any[], options?: Interface.optionsInterface): KoaRouter.IMiddleware;
export declare class Router {
    static basePath(route: string): (target: any) => void;
    static route(method: Constant.HttpMethod, route: string | [string], ...middleware: Interface.MiddlewareInterface[]): Interface.DecoratorInterface;
    static get(route: string | [string], ...middleware: Interface.MiddlewareInterface[]): Interface.DecoratorInterface;
    static post(route: string | [string], ...middleware: Interface.MiddlewareInterface[]): Interface.DecoratorInterface;
    static put(route: string | [string], ...middleware: Interface.MiddlewareInterface[]): Interface.DecoratorInterface;
    static delete(route: string | [string], ...middleware: Interface.MiddlewareInterface[]): Interface.DecoratorInterface;
}
