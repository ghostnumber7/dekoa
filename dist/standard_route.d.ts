import { Context } from 'koa';
export interface StandardRouteInterface {
    list?(ctx: Context): Promise<any>;
    get?(ctx: Context): Promise<any>;
    create?(ctx: Context): Promise<any>;
    update?(ctx: Context): Promise<any>;
    delete?(ctx: Context): Promise<any>;
}
export declare class StandardRoute implements StandardRouteInterface {
    list(ctx: Context): Promise<any>;
    get(ctx: Context): Promise<any>;
    create(ctx: Context): Promise<any>;
    update(ctx: Context): Promise<void>;
    delete(ctx: Context): Promise<any>;
}
