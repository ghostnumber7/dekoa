import koa from 'koa'
import dekoa, {Router, Database, StandardRoute, Middleware} from '../dist/dekoa'
import bodyParser from 'koa-bodyparser'

class Test extends StandardRoute {
  @Router.get('/', Middleware.QueryStringToFindCriteria)
  async list(ctx) {
    return super.list(ctx)
  }

  @Router.get('/:id')
  async get(ctx) {
    return super.get(ctx)
  }

  @Router.post('/', Middleware.RequireValue('request.body.name'))
  async create(ctx) {
    return super.create(ctx)
  }

  @Router.put('/:id')
  async update(ctx) {
    return super.update(ctx)
  }

  @Router.delete('/:id')
  async delete(ctx) {
    return super.delete(ctx)
  }
}

var app = new koa()
app.use(bodyParser())
app.use(dekoa(app, [Test], {
  connector: new Database.Memory,
  errorHandler: (ctx) => { // Will be called on error
    ctx.status = ctx.state.error.status || 500
    ctx.body = `ERROR: ${ctx.state.error.message} AT ${ctx.state.dekoa.className} -> ${ctx.state.dekoa.method}`
  }
}))
app.use((ctx) => { // No matching route
  ctx.body = "404 not found"
})
app.listen({port: 8080, host: '0.0.0.0'}, function() {
  console.log("example server running on 0.0.0.0:8080")
})